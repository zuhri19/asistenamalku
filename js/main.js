$(document).ready(function() {



    $(window).scroll(function(){
        if ($(window).scrollTop() >= 80) {
            $('nav').addClass('fixed-top-main');
            $('nav div').addClass('visible-title');
        }
        else {
            $('nav').removeClass('fixed-top-main');
            $('nav div').removeClass('visible-title');
        }
    });

      // Paralax one
      var controller = new ScrollMagic.Controller();

      // Build Scene
      var ourScene = new ScrollMagic.Scene({
          triggerElement:'#home01'
      })
      .setClassToggle('#home01','home__img--fadein')
      .addTo(controller);

     // Paralax one
     var controller = new ScrollMagic.Controller();

     // Build Scene
     var ourScene = new ScrollMagic.Scene({
         triggerElement:'#opening01'
     })
     .setClassToggle('#opening01','opening__img--fadein')
     .addTo(controller);

    // Paralax two
    var controller = new ScrollMagic.Controller();

    // Build Scene
    var ourScene = new ScrollMagic.Scene({
        triggerElement:'#mission2'
    })
    .setClassToggle('#mission2','mission__img--fadein')
    .addTo(controller);

     // Paralax tree
     var controller = new ScrollMagic.Controller();

     // Build Scene
     var ourScene = new ScrollMagic.Scene({
         triggerElement:'#evaluasi01'
     })
     .setClassToggle('#evaluasi01','evaluasi__img--fadein')
     .addTo(controller);

      // Paralax four
      var controller = new ScrollMagic.Controller();

      // Build Scene
      var ourScene = new ScrollMagic.Scene({
          triggerElement:'#finding01'
      })
      .setClassToggle('#finding01','finding__img--fadein')
      .addTo(controller);

      // Paralax five
      var controller = new ScrollMagic.Controller();

      // Build Scene
      var ourScene = new ScrollMagic.Scene({
          triggerElement:'#calender01'
      })
      .setClassToggle('#calender01','calender__img--fadein')
      .addTo(controller);

        
    $('.owl-carousel').owlCarousel({
        responsiveClass:true,
        autoplayTimeout:2000,
        autoplay:true,
        loop:true,
        responsive:{
            320:{
                items:1
            },
            375:{
                items:1
            },
            441:{
                items:1
            },
            1000:{
                items:2
            },
        }
    })
    


    // Polygon
    /* ==========================================================================
        Particle
    ========================================================================== */ 
        particlesJS('particles-js',{
            "particles": {
            "number": {
            "value": 170,
            "density": {
                "enable": true,
                "value_area": 800
            }
            },
            "color": {
            "value": ["#7277F5","#60CDFF","#FF98BE","#34BFFF","#FFFFFF"]
            },
            "shape": {
            "type": "circle",
            "stroke": {
                "width": 0,
                "color": "#000000"
            },
            "polygon": {
                "nb_sides": 3
            },
            "image": {
                "src": "img/github.svg",
                "width": 100,
                "height": 100
            }
            },
            "opacity": {
            "value": 1,
            "random": true,
            "anim": {
                "enable": true,
                "speed": 1,
                "opacity_min": 0,
                "sync": false
            }
            },
            "size": {
            "value": 5,
            "random": true,
            "anim": {
                "enable": false,
                "speed": 4,
                "size_min": 0.3,
                "sync": false
            }
            },
            "line_linked": {
            "enable": false,
            "distance": 150,
            "color": "#ffffff",
            "opacity": 0.4,
            "width": 1
            },
            "move": {
            "enable": true,
            "speed": 5,
            "direction": "top-left",
            "random": true,
            "straight": false,
            "out_mode": "bounce",
            "bounce": false,
            "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 600
            }
            }
        },
        "interactivity": {
            "detect_on": "canvas",
            "events": {
            "onhover": {
                "enable": false,
                "mode": "bubble"
            },
            "onclick": {
                "enable": true,
                "mode": "repulse"
            },
            "resize": true
            },
            "modes": {
            "grab": {
                "distance": 400,
                "line_linked": {
                "opacity": 1
                }
            },
            "bubble": {
                "distance": 350,
                "size": 0,
                "duration": 2,
                "opacity": 0,
                "speed": 3
            },
            "repulse": {
                "distance": 400,
                "duration": 0.4
            },
            "push": {
                "particles_nb": 4
            },
            "remove": {
                "particles_nb": 2
            }
            }
        },
        "retina_detect": true
        }

    );

        // Patikeljs Mobile
        particlesJS('particles-jss',{
            "particles": {
            "number": {
            "value": 170,
            "density": {
                "enable": true,
                "value_area": 800
            }
            },
            "color": {
            "value": ["#7277F5","#60CDFF","#FF98BE","#34BFFF","#FFFFFF"]
            },
            "shape": {
            "type": "circle",
            "stroke": {
                "width": 0,
                "color": "#000000"
            },
            "polygon": {
                "nb_sides": 3
            },
            "image": {
                "src": "img/github.svg",
                "width": 100,
                "height": 100
            }
            },
            "opacity": {
            "value": 1,
            "random": true,
            "anim": {
                "enable": true,
                "speed": 1,
                "opacity_min": 0,
                "sync": false
            }
            },
            "size": {
            "value": 5,
            "random": true,
            "anim": {
                "enable": false,
                "speed": 4,
                "size_min": 0.3,
                "sync": false
            }
            },
            "line_linked": {
            "enable": false,
            "distance": 150,
            "color": "#ffffff",
            "opacity": 0.4,
            "width": 1
            },
            "move": {
            "enable": true,
            "speed": 5,
            "direction": "top-left",
            "random": true,
            "straight": false,
            "out_mode": "bounce",
            "bounce": false,
            "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 600
            }
            }
        },
        "interactivity": {
            "detect_on": "canvas",
            "events": {
            "onhover": {
                "enable": false,
                "mode": "bubble"
            },
            "onclick": {
                "enable": true,
                "mode": "repulse"
            },
            "resize": true
            },
            "modes": {
            "grab": {
                "distance": 400,
                "line_linked": {
                "opacity": 1
                }
            },
            "bubble": {
                "distance": 350,
                "size": 0,
                "duration": 2,
                "opacity": 0,
                "speed": 3
            },
            "repulse": {
                "distance": 400,
                "duration": 0.4
            },
            "push": {
                "particles_nb": 4
            },
            "remove": {
                "particles_nb": 2
            }
            }
        },
        "retina_detect": true
        }

    );

    // Partikel Evaluasi
        // Patikeljs Mobile
        particlesJS('evaluasimobile',{
            "particles": {
            "number": {
            "value": 170,
            "density": {
                "enable": true,
                "value_area": 800
            }
            },
            "color": {
            "value": ["#7277F5","#60CDFF","#FF98BE","#34BFFF","#FFFFFF"]
            },
            "shape": {
            "type": "circle",
            "stroke": {
                "width": 0,
                "color": "#000000"
            },
            "polygon": {
                "nb_sides": 3
            },
            "image": {
                "src": "img/github.svg",
                "width": 100,
                "height": 100
            }
            },
            "opacity": {
            "value": 1,
            "random": true,
            "anim": {
                "enable": true,
                "speed": 1,
                "opacity_min": 0,
                "sync": false
            }
            },
            "size": {
            "value": 5,
            "random": true,
            "anim": {
                "enable": false,
                "speed": 4,
                "size_min": 0.3,
                "sync": false
            }
            },
            "line_linked": {
            "enable": false,
            "distance": 150,
            "color": "#ffffff",
            "opacity": 0.4,
            "width": 1
            },
            "move": {
            "enable": true,
            "speed": 5,
            "direction": "top-left",
            "random": true,
            "straight": false,
            "out_mode": "bounce",
            "bounce": false,
            "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 600
            }
            }
        },
        "interactivity": {
            "detect_on": "canvas",
            "events": {
            "onhover": {
                "enable": false,
                "mode": "bubble"
            },
            "onclick": {
                "enable": true,
                "mode": "repulse"
            },
            "resize": true
            },
            "modes": {
            "grab": {
                "distance": 400,
                "line_linked": {
                "opacity": 1
                }
            },
            "bubble": {
                "distance": 350,
                "size": 0,
                "duration": 2,
                "opacity": 0,
                "speed": 3
            },
            "repulse": {
                "distance": 400,
                "duration": 0.4
            },
            "push": {
                "particles_nb": 4
            },
            "remove": {
                "particles_nb": 2
            }
            }
        },
        "retina_detect": true
        }

    );

    // Scroll

    $(document).ready(function(){
        var top = ('nav');
        $('a[href^="#"]').on('click',function (e) {
            e.preventDefault();
    
            var target = this.hash;
            var $target = $(target);
    
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
    
            }, 900, 'swing', function () {
                window.location.hash = target;
            });
        });
    });
    
});
